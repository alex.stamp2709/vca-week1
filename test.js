//first NodeJS application...This is a simple application that will create a restful webservice listening on port 3000. 
const express = require('express') // Link the application to the express framework (this is a library that you will need to install inside the project).
const app = express() //Create an instance of an express restful service.
const port = 3000 //Define a port number for the service to listen on.

//  Use the method get to create a hook on the server for get requests to the / (aka root)
//directory resource (first parameter of the get method). Also pass to get the method to be called
//when the resource is accessed (req,res) => {}.  Inside the function we access the res object to
//send the the text ‘hello virtual machine’
app.get('/', (req, res) => {
 res.send('Hello Virtual Machine!')
})

//use the listen method to bind to the port, specified as the first argument (3000).
//For the second argument we pass in a method that calls console.log() to print some status to
//output of the terminal.
app.listen(port, () => {
 console.log(`Express Application listening at port 3000`)
})